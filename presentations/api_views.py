from django.http import JsonResponse
from .models import Presentation, Status
from events.models import Conference
from common.json import ModelEncoder
from django.view.decorators.http import require_http_methods
import json


class StatusEncoder(ModelEncoder):
    model = Status
    properties = ["name"]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title", "status"]

    encoders = {"status": StatusEncoder()}


class ConferenceEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "status",
        "conference",
    ]
    encoders = {
        "status": StatusEncoder(),
        "conference": ConferenceEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            status = Status.objects.get(name=content["status"]["name"])
            content["status"] = status
        except Status.DoesNotExist:
            return JsonResponse({"message": "Invalid status name"}, status=400)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}, status=400
            )
        presentation = Presentation.objects.create(**content)
        return JsonResponse(
            presentation, encoder=PresentationDetailEncoder, safe=False
        )


@require_http_methods(["GET", "POST"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentations = Presentation.objects.get(id=id)
        return JsonResponse(
            presentations, encoder=PresentationDetailEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            status = Status.objects.get(name=content["status"]["name"])
            content["status"] = status
        except Status.DoesNotExist:
            return JsonResponse({message: "Invalid status name"}, status=400)
        try:
            conference = Conference.objects.get(id=id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}, status=400
            )
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return Jsonresponse(
            {"presentation": presentation}, encoder=PresentationDetailEncoder
        )


def approved_status(request, id):
    if request.method == "POST":
        status = Presentation.objects.get(id=id)
        status.approve()
        return JsonResponse({"approve": "APPROVED"})


def rejected_status(request, id):
    if request.method == "POST":
        status = Presentation.objects.get(id=id)
        status.reject()
        return JsonResponse({"approve": "REJECTED"})
