from django.http import JsonResponse

from .models import Attendee
from events.models import Conference

from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference_id=conference_id)
        return JsonResponse(
            {"attendees": "attendees"}, encoder=AttendeeListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["confernece"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}, status=400
            )
        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee, encoder=AttendeeDetailsEncoder, safe=False
        )


class ConferenceEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class AttendeeDetailsEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceEncoder(),
    }


@require_http_methods(["GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendees = Attendee.objects.get(id=id)
        return JsonResponse(
            attendees, encoder=AttendeeDetailsEncoder, safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"}, status=400
            )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            {"attendee": attendee}, encoder=AttendeeDetailsEncoder
        )


def add_badge(request, id):
    if request.method == "POST":
        attendee = Attendee.objects.get(id=id)
        attendee.create_badge()
        return JsonResponse({"badge": True})
